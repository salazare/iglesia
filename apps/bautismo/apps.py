from django.apps import AppConfig


class BautizmoConfig(AppConfig):
    name = 'bautismo'
