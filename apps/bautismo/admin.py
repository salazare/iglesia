from django.contrib import admin
from .models import Ministro, Libro, Acta, TipoMinistro


class TipoMinistroAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'abreviatura',)
    search_fields = ['nombre', 'abreviatura']


class MinistroAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'tipo', 'activo',)
    list_filter = ('activo', 'tipo')
    search_fields = ['nombre', 'apellido']


class LibroAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'f_inicio', 'f_fin', 'activo',)
    list_filter = ('activo',)
    search_fields = ['nombre']


class ActaAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'apellido', 'f_bautizo', 'ministro')
    list_filter = ('libro', 'sexo', 'ministro')
    search_fields = ['nombre', 'apellido']


admin.site.register(TipoMinistro, TipoMinistroAdmin)
admin.site.register(Ministro, MinistroAdmin)
admin.site.register(Libro, LibroAdmin)
admin.site.register(Acta, ActaAdmin)
