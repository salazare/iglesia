from django.db import models

SEXOS_LIST = [
    ('F', 'Femenino'),
    ('M', 'Masculino'),
]


class TimestampModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TipoMinistro(models.Model):
    nombre = models.CharField(max_length=50)
    abreviatura = models.CharField(max_length=10)

    def __str__(self):
        return self.nombre


class Ministro(TimestampModel):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    tipo = models.ForeignKey(TipoMinistro, on_delete=models.SET_NULL, null=True)
    activo = models.BooleanField(default=False)

    def __str__(self):
        return '%s %s' % (self.nombre, self.apellido)

    @property
    def full_name(self):
        return '%s %s' % (self.nombre, self.apellido)

    class Meta:
        ordering = ["nombre"]


class Libro(TimestampModel):
    nombre = models.CharField(max_length=50)
    activo = models.BooleanField(default=False)
    f_inicio = models.DateField('Fecha Inicio', blank=True, null=True)
    f_fin = models.DateField('Fecha Fin', blank=True, null=True)

    def __str__(self):
        return self.nombre

    class Meta:
        ordering = ["nombre"]


class Acta(TimestampModel):
    libro = models.ForeignKey(Libro, on_delete=models.SET_NULL, blank=True, null=True, )
    folio = models.IntegerField()
    num = models.IntegerField(blank=True, null=True)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    sexo = models.CharField(max_length=1, choices=SEXOS_LIST)
    f_nacimiento = models.DateField('Fecha de nacimiento', blank=True, null=True)
    lugar_nacimiento = models.CharField('Lugar de nacimiento', max_length=50, blank=True, null=True)
    cedula = models.CharField(max_length=15, blank=True, null=True)
    padre_nombre = models.CharField('Nombre del padre', max_length=50, blank=True, null=True)
    padre_apellido = models.CharField('Apellido del padre', max_length=50, blank=True, null=True)
    madre_nombre = models.CharField('Nombre de la madre', max_length=50, blank=True, null=True)
    madre_apellido = models.CharField('Apellido de la madre', max_length=50, blank=True, null=True)
    filiacion = models.CharField(max_length=20, blank=True, null=True)
    f_bautizo = models.DateField('Fecha de bautizo')
    padrino_nombre = models.CharField('Nombre del padrino', max_length=50, blank=True, null=True)
    padrino_apellido = models.CharField('Apellido del padrino', max_length=50, blank=True, null=True)
    madrina_nombre = models.CharField('Nombre de la madrina', max_length=50, blank=True, null=True)
    madrina_apellido = models.CharField('Apellido de la madrina', max_length=50, blank=True, null=True)
    ministro = models.ForeignKey(Ministro, on_delete=models.SET_NULL, blank=True, null=True, )
    observacion = models.TextField(blank=True, null=True)
    casado = models.BooleanField(default=False)
    f_matrimonio = models.DateField('Fecha de matrimonio', blank=True, null=True)
    conyugue = models.CharField('Nombre del conyugue', max_length=100, blank=True, null=True)
    parroquia_matrimonio = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.nombre, self.apellido)

    @property
    def full_name(self):
        return '%s %s' % (self.nombre, self.apellido)

    @property
    def padrino(self):
        return '%s %s' % (self.padrino_nombre, self.padrino_apellido)

    @property
    def madrina(self):
        return '%s %s' % (self.madrina_nombre, self.madrina_apellido)

    @property
    def padre(self):
        return '%s %s' % (self.padre_nombre, self.padre_apellido)

    @property
    def madre(self):
        return '%s %s' % (self.madre_nombre, self.madre_apellido)

    class Meta:
        ordering = ["nombre"]
