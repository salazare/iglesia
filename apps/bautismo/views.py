from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin
from weasyprint import HTML
from weasyprint.fonts import FontConfiguration
from django.http import HttpResponse
from django.utils.text import slugify
from django.conf import settings
from django.template.loader import render_to_string
from .models import Ministro, Libro, Acta
from .forms import MinistroForm, LibroForm, ActaForm, ActaSearchForm
import datetime


class HomeView(TemplateView):
    template_name = "index.html"


class MinistroList(ListView):
    model = Ministro
    ordering = ('nombre', 'apellido',)


class MinistroCreate(SuccessMessageMixin, CreateView):
    model = Ministro
    form_class = MinistroForm
    success_url = reverse_lazy('bautismo:ministros-list')
    success_message = "Ministro creado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Nuevo ministro'
        return context


class MinistroUpdate(SuccessMessageMixin, UpdateView):
    model = Ministro
    form_class = MinistroForm
    success_url = reverse_lazy('bautismo:ministros-list')
    success_message = "Ministro actualizado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Editar ministro'
        return context


class LibroList(ListView):
    model = Libro
    ordering = ('nombre',)


class LibroCreate(SuccessMessageMixin, CreateView):
    model = Libro
    form_class = LibroForm
    success_url = reverse_lazy('bautismo:libros-list')
    success_message = "Libro creado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Nuevo libro'
        return context


class LibroUpdate(SuccessMessageMixin, UpdateView):
    model = Libro
    form_class = LibroForm
    success_url = reverse_lazy('bautismo:libros-list')
    success_message = "Libro actualizado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Editar libro'
        return context


class ActaList(ListView):
    model = Acta
    ordering = ('nombre', 'apellido',)
    paginate_by = 15
    form = ActaSearchForm

    def get_queryset(self):
        queryset = Acta.objects.all()
        form_search = self.form(self.request.GET)
        if form_search.is_valid():
            data = form_search.cleaned_data
            if data['nombre']:
                queryset = queryset.filter(nombre=data['nombre'])
            if data['apellido']:
                queryset = queryset.filter(apellido=data['apellido'])
            if data['f_bautizo']:
                queryset = queryset.filter(f_bautizo=data['f_bautizo'])
        else:
            print('formulario no válido')
        return queryset

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['form_search'] = self.form(self.request.GET)
        return context


class ActaCreate(SuccessMessageMixin, CreateView):
    model = Acta
    form_class = ActaForm
    success_url = reverse_lazy('bautismo:actas-list')
    success_message = "Acta de bautismo creado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Agregar acta de bautismo'
        return context


class ActaUpdate(SuccessMessageMixin, UpdateView):
    model = Acta
    form_class = ActaForm
    success_url = reverse_lazy('bautismo:actas-list')
    success_message = "Acta de bautismo actualizado con exito"

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['title'] = 'Editar acta de bautismo'
        return context


def actaToPdf(request, pk):
    # return render(request, 'bautismo/acta_pdf.html')
    acta = get_object_or_404(Acta, pk=pk)
    hoy = datetime.date.today()
    ministro = get_object_or_404(Ministro, activo=True)
    namefile = slugify('bautismo ' + acta.apellido + ' ' + acta.nombre)
    # response['Content-Disposition'] = "inline; filename={namefile}.pdf".format(
    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = "inline; filename={namefile}.pdf".format(namefile=namefile)
    html = render_to_string("bautismo/acta_pdf.html", {'acta': acta, 'hoy': hoy, 'ministro': ministro})
    font_config = FontConfiguration()
    HTML(string=html, base_url=request.build_absolute_uri()).write_pdf(response, font_config=font_config)
    return response


def setMinistroDefault(request, pk):
    ministros = Ministro.objects.all()
    for ministro in ministros:
        if ministro.pk == pk:
            ministro.activo = True
        else:
            ministro.activo = False
        ministro.save()
    return redirect(reverse_lazy('bautismo:ministros-list'))
