from django.forms import ModelForm

from django import forms
from .models import Ministro, Libro, Acta


class MinistroForm(ModelForm):
    nombre = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    apellido = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Apellido'}))

    class Meta:
        model = Ministro
        exclude = ['created_at', 'updated_at', 'activo']


class LibroForm(ModelForm):
    class Meta:
        model = Libro
        exclude = ['created_at', 'updated_at']


class ActaForm(ModelForm):
    class Meta:
        model = Acta
        exclude = ['created_at', 'updated_at']


class ActaSearchForm(forms.Form):
    nombre = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': 'Nombre'}))
    apellido = forms.CharField(max_length=50, required=False, widget=forms.TextInput(attrs={'placeholder': 'Apellido'}))
    f_bautizo = forms.DateField(required=False, label='Fecha de Bautizo', widget=forms.TextInput(attrs={'placeholder': 'Bautizo'}))
