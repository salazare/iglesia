from django.urls import path

from .views import HomeView, MinistroList, MinistroCreate, MinistroUpdate, LibroList, LibroCreate, LibroUpdate, \
    ActaList, ActaCreate, ActaUpdate, actaToPdf, setMinistroDefault

app_name = 'bautismo'
urlpatterns = [
    path('', HomeView.as_view(), name='index'),

    path('ministros/', MinistroList.as_view(), name='ministros-list'),
    path('ministros/nuevo', MinistroCreate.as_view(), name='ministros-nuevo'),
    path('ministros/editar/<int:pk>', MinistroUpdate.as_view(), name='ministros-editar'),
    path('actas/set-default/<int:pk>', setMinistroDefault, name='ministros-default'),

    path('libros/', LibroList.as_view(), name='libros-list'),
    path('libros/nuevo', LibroCreate.as_view(), name='libros-nuevo'),
    path('libros/editar/<int:pk>', LibroUpdate.as_view(), name='libros-editar'),

    path('actas/', ActaList.as_view(), name='actas-list'),
    path('actas/nuevo', ActaCreate.as_view(), name='actas-nuevo'),
    path('actas/editar/<int:pk>', ActaUpdate.as_view(), name='actas-editar'),
    path('actas/pdf/<int:pk>', actaToPdf, name='acta-pdf'),

]
